import { Injectable } from '@angular/core';
import { flickrConfig }  from '../configs/flickr.config.js';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class FlickrService {
  page: number = 0;
  per_page: number = 20;
  
  constructor(private http: HttpClient) { }

  getAllPhotos(query, reset): Observable<any> {
    this.page++;
    let q = query? '&tags=' + query : ''
    console.log(q);
    if(reset) this.page = 1;
    console.log(this.page);
    return this.http.get(`https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=${flickrConfig.API_KEY}&text=dogs&format=json&per_page=${this.per_page}&page=${this.page}${q}&nojsoncallback=1`);
  }

  getPhoto(photo): Observable<any> {
    return this.http.get(`https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}.jpg`, {responseType: 'blob'});
  }

}


