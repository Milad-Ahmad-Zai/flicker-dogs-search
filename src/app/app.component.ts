import { Component } from '@angular/core';
import { FlickrService } from './services/flickr.service';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Flickr Photo Search';
  photos = [];
  loading = false;
  searchTerm = "";
  searchQuery = "";
  constructor(private flickerService: FlickrService, private sanitizer: DomSanitizer){}

  ngOnInit(){
    this.loadImages(false);
  }

  loadImages(reset = false){
    console.log(this.searchQuery, reset);
    this.loading = true;
    this.flickerService.getAllPhotos(this.searchQuery, reset).subscribe(data => {
      data.photos.photo.map(photo =>{
        if(reset) this.photos = [];
        this.flickerService.getPhoto(photo).subscribe(res => {
          this.photos.push(res);
          this.loading = false;
          //console.log(this.photos);
        });
      });
    });
  }

  generateImage(photo){
    return this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(photo));
  }
  
  onScroll(){
    this.loadImages(false);
  }

  doSearch(){
    this.searchQuery = this.searchTerm;
    this.loadImages(true);
  }

}
