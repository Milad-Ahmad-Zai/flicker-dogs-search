# FlickrPhotoSearch

Angular 6 Flicker dogs image gallery with search functionality.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## TODO

### Performance optimization.
Flicker API returns image blobs instead of image paths, which significantly reduces performance once image numbers increases.

